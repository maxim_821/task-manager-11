package ru.ivanov.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void updateTaskByIndex();

    void updateTaskById();
}
